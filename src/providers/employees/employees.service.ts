// eslint-disable-next-line prettier/prettier
import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { EmployeesInput } from '../../graphql/employees/dtos/input.dto';
import IEmployees from '../../models/employees.model';

// eslint-disable-next-line prettier/prettier
@Injectable()
export class EmployeesService {
  constructor(
    @InjectModel('Employee') private readonly model: Model<IEmployees>,
  ) {}

  async addNewEmployee(employee: EmployeesInput) {
    try {
      const newEmployee = new this.model(employee);
      return await newEmployee.save();
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async getEmployees() {
    try {
      const employees = await this.model.find().populate('department');
      return employees as IEmployees[];
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
  async getEmployee(id: string) {
    try {
      const employee = await this.model.findById(id).populate('department');
      if (!employee) {
        throw new NotFoundException();
      }
      return employee as IEmployees;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async updateExistingEmployee(id: string, updates: EmployeesInput) {
    try {
      const employee = await this.model.findByIdAndUpdate(id, updates, {
        new: true,
      });
      if (!employee) {
        throw new NotFoundException();
      }
      return (await employee
        .save()
        .then((employee) => employee)
        .catch((err) => err)) as IEmployees;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async deleteEmployee(id: string) {
    try {
      const deletedEmployee = await this.model.findByIdAndDelete(id);
      if (!deletedEmployee) {
        throw new NotFoundException();
      }
      return deletedEmployee;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async getEmployeesByDepartmentId(id: string) {
    return (await this.model.find({ department: `${id}` })) as IEmployees[];
  }
}
