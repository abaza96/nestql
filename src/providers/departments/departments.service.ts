// eslint-disable-next-line prettier/prettier
import { Injectable, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { DepartmentsInput } from '../../graphql/departments/dtos/input.dto';
import IDepartments from '../../models/department.model';

@Injectable()
export class DepartmentsService {
  constructor(
    @InjectModel('Department') private readonly model: Model<IDepartments>,
  ) {}

  async addDepartment(department: DepartmentsInput) {
    try {
      const newDepartment = new this.model(department);
      return (await newDepartment.save()) as IDepartments;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async getDepartments() {
    try {
      const departments = await this.model.find().populate('employees');
      return departments as IDepartments[];
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
  async getDepartment(id: string) {
    try {
      const department = await this.model.findById(id).populate('employees');
      if (!department) {
        throw new NotFoundException();
      }
      return department as IDepartments;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async updateExistingDepartment(id: string, updates: DepartmentsInput) {
    try {
      const department = await this.model.findByIdAndUpdate(id, updates, {
        new: true,
      });
      if (!department) {
        throw new NotFoundException();
      }
      return (await department.save()) as IDepartments;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }

  async deleteDepartment(id: string) {
    try {
      const deletedDepartment = await this.model.findByIdAndDelete(id);
      if (!deletedDepartment) {
        throw new NotFoundException();
      }
      return deletedDepartment;
    } catch (error) {
      throw new InternalServerErrorException(error);
    }
  }
}
