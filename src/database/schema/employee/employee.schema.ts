import { Schema } from 'mongoose';

export const EmployeeSchema: Schema = new Schema(
  {
    firstName: { type: String, required: true },
    lastName: { type: String, required: true },
    contactNumber: { type: String, required: true },
    salary: { type: Number, required: true },
    department: { type: Schema.Types.ObjectId, ref: 'Department' },
  },
  { timestamps: true },
);
