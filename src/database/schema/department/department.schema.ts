import { Schema } from 'mongoose';

export const DepartmentSchema: Schema = new Schema(
  {
    departmentName: { type: String, required: true },
    departmentNumber: { type: Number, required: true },
    employees: [{ type: Schema.Types.ObjectId, ref: 'Employee' }],
  },
  { timestamps: true },
);
