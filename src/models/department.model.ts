import { Document } from 'mongoose';

import IEmployees from './employees.model';

export default interface IDepartments extends Document {
  id: string;
  departmentName: string;
  departmentNumber: number;
  employees: [IEmployees];
}
