import { Document } from 'mongoose';

import IDepartments from './department.model';

export default interface IEmployees extends Document {
  id: string;
  firstName: string;
  lastName: string;
  contactNumber: string;
  salary: number;
  department: IDepartments;
}
