import { ApolloDriver } from '@nestjs/apollo';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { MongooseModule } from '@nestjs/mongoose';

import { DepartmentsModule } from './controllers/departments/departments.module';
import { EmployeesModule } from './controllers/employees/employees.module';

@Module({
  imports: [
    GraphQLModule.forRoot({
      driver: ApolloDriver,
      autoSchemaFile: 'schema.gql',
      path: '/v2',
    }),
    MongooseModule.forRoot('mongodb://localhost:27017/nestQL'),
    EmployeesModule,
    DepartmentsModule,
  ],
})
export class AppModule {}
