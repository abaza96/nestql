import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { DepartmentSchema } from '../../database/schema/department/department.schema';
import { DepartmentsResolver } from '../../graphql/departments/departments.resolver';
import { DepartmentsService } from '../../providers/departments/departments.service';
import { DepartmentsController } from './departments.controller';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: 'Department', schema: DepartmentSchema },
    ]),
  ],
  controllers: [DepartmentsController],
  providers: [DepartmentsService, DepartmentsResolver],
})
export class DepartmentsModule {}
