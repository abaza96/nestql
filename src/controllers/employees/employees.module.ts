import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { EmployeesService } from 'src/providers/employees/employees.service';

import { EmployeeSchema } from '../../database/schema/employee/employee.schema';
import { EmployeesResolver } from '../../graphql/employees/employees.resolver';
import { EmployeesController } from './employees.controller';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Employee', schema: EmployeeSchema }]),
  ],
  controllers: [EmployeesController],
  providers: [EmployeesService, EmployeesResolver],
})
export class EmployeesModule {}
