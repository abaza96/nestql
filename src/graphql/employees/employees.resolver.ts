import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { EmployeesService } from '../../providers/employees/employees.service';
import { EmployeesInput } from './dtos/input.dto';
import { EmployeesObject } from './dtos/object.dto';

@Resolver()
export class EmployeesResolver {
  constructor(private readonly svc: EmployeesService) {}

  @Query(() => [EmployeesObject])
  async getAllEmployees() {
    return await this.svc.getEmployees();
  }

  @Query(() => EmployeesObject)
  async getOneEmployee(@Args('id') id: string) {
    return await this.svc.getEmployee(id);
  }

  @Mutation(() => EmployeesObject)
  async createEmployee(@Args('input') input: EmployeesInput) {
    return await this.svc.addNewEmployee(input);
  }

  @Mutation(() => EmployeesObject)
  async updateEmployee(
    @Args('input') input: EmployeesInput,
    @Args('id') id: string,
  ) {
    return await this.svc.updateExistingEmployee(id, input);
  }

  @Mutation(() => EmployeesObject)
  async deleteEmployee(@Args('id') id: string) {
    return await this.svc.deleteEmployee(id);
  }
}
