import { Field, InputType, Int } from '@nestjs/graphql';

@InputType()
export class EmployeesInput {
  @Field()
  readonly firstName: string;
  @Field()
  readonly lastName: string;
  @Field()
  readonly contactNumber: string;
  @Field(() => Int)
  readonly salary: number;
}
