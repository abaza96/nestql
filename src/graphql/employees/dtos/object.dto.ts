import { Field, ID, Int, ObjectType } from '@nestjs/graphql';

import { DepartmentsObject } from '../../../graphql/departments/dtos/object.dto';

@ObjectType()
export class EmployeesObject {
  @Field(() => ID)
  readonly id: string;
  @Field()
  readonly firstName: string;
  @Field()
  readonly lastName: string;
  @Field()
  readonly contactNumber: string;
  @Field(() => Int)
  readonly salary: number;
  @Field(() => DepartmentsObject, { nullable: true })
  readonly department: DepartmentsObject;
}
