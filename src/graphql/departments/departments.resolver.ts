import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { DepartmentsService } from '../../providers/departments/departments.service';
import { DepartmentsInput } from './dtos/input.dto';
import { DepartmentsObject } from './dtos/object.dto';

@Resolver()
export class DepartmentsResolver {
  constructor(private readonly svc: DepartmentsService) {}

  @Query(() => [DepartmentsObject])
  async getAllDepartments() {
    return await this.svc.getDepartments();
  }

  @Query(() => DepartmentsObject)
  async getOneDepartment(@Args('id') id: string) {
    return await this.svc.getDepartment(id);
  }

  @Mutation(() => DepartmentsObject)
  async createDepartment(@Args('input') input: DepartmentsInput) {
    return await this.svc.addDepartment(input);
  }

  @Mutation(() => DepartmentsObject)
  async updateDepartment(
    @Args('input') input: DepartmentsInput,
    @Args('id') id: string,
  ) {
    return await this.svc.updateExistingDepartment(id, input);
  }

  @Mutation(() => DepartmentsObject)
  async deleteDepartment(@Args('id') id: string) {
    return await this.svc.deleteDepartment(id);
  }
}
