import { Field, ID, Int, ObjectType } from '@nestjs/graphql';

import { EmployeesObject } from '../../../graphql/employees/dtos/object.dto';

@ObjectType()
export class DepartmentsObject {
  @Field(() => ID)
  readonly id: string;
  @Field()
  readonly departmentName: string;
  @Field(() => Int)
  readonly departmentNumber: number;
  @Field(() => [EmployeesObject], { nullable: true })
  readonly employees: EmployeesObject[];
}
