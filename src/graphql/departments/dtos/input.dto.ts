import { Field, InputType, Int } from '@nestjs/graphql';

@InputType()
export class DepartmentsInput {
  @Field()
  readonly departmentName: string;
  @Field(() => Int)
  readonly departmentNumber: number;
}
